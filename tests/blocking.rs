#[test]
fn threaded_multi_u32() {
    use bombs::blocking::MultiBomb;
    use std::thread;

    let (mut fuse, bomb) = MultiBomb::new();

    let handles = (0..2).into_iter().map(move |_| {
        let mut bomb_clone = bomb.clone();

        thread::spawn(move || {
            let value = bomb_clone.wait_for_explosion();
            assert_eq!(value, 5);
            let value = bomb_clone.wait_for_explosion();
            assert_eq!(value, 24);
        })
    }).collect::<Vec<_>>();

    let flame = fuse.light(5_u32);

    flame.wait_for_extinguish();
    assert!(flame.extinguished());

    let flame = fuse.light(24);

    flame.wait_for_extinguish();
    assert!(flame.extinguished());

    handles.into_iter().for_each(|h| h.join().unwrap());
}

#[test]
fn threaded_u32() {
    use bombs::blocking::Bomb;
    use std::thread;

    let (fuse, bomb) = Bomb::new();

    let handles = (0..2).into_iter().map(move |_| {
        let bomb_clone = bomb.clone();

        thread::spawn(move || {
            let value = bomb_clone.wait_for_explosion();
            assert_eq!(*value, 5);
        })
    }).collect::<Vec<_>>();

    let flame = fuse.light(5_u32);

    flame.wait_for_extinguish();
    assert!(flame.extinguished());

    handles.into_iter().for_each(|h| h.join().unwrap());
}

#[test]
fn simple_u32() {
    use bombs::blocking::Bomb;

    let (fuse, bomb) = Bomb::new();

    let bomb_clone = bomb.clone();

    assert_eq!(bomb.exploded(), None);
    assert_eq!(bomb_clone.exploded(), None);

    let flame = fuse.light(7_u32);

    assert_eq!(bomb.exploded(), Some(&7));
    assert_eq!(bomb_clone.wait_for_explosion(), &7);

    assert!(!flame.extinguished());

    drop(bomb_clone);
    drop(bomb);

    assert!(flame.extinguished());
}

#[test]
fn empty_u32() {
    use bombs::blocking::Bomb;

    let (fuse, bomb) = Bomb::<u32>::new();
    let bomb_clone = bomb.clone();

    assert_eq!(bomb.exploded(), None);
    assert_eq!(bomb_clone.exploded(), None);

    let _fuse = std::hint::black_box(fuse);
}
