#![cfg(loom)]

#[path = "loom/basic.rs"]
mod basic;

#[path = "loom/blocking.rs"]
mod blocking;