//! Types for loom.

pub mod sync {
    pub mod atomic {
        #[cfg(not(loom))]
        pub use std::sync::atomic::{ AtomicPtr, AtomicBool, Ordering };
        #[cfg(loom)]
        pub use loom::sync::atomic::{ AtomicPtr, AtomicBool, Ordering };
    }

    #[cfg(not(loom))]
    pub use std::sync::Arc;
    #[cfg(loom)]
    pub use loom::sync::Arc;

}

pub mod cell {
    #[cfg(not(loom))]
    pub use std::cell::{ Cell, UnsafeCell };
    #[cfg(loom)]
    pub use loom::cell::{ Cell, UnsafeCell };
}

pub mod thread {
    #[cfg(not(loom))]
    pub use std::thread::*;
    #[cfg(loom)]
    pub use loom::thread::*;
}

pub mod hint {
    #[cfg(not(loom))]
    pub use std::hint::spin_loop;
    #[cfg(loom)]
    pub use loom::hint::spin_loop;
}