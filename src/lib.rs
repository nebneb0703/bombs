//! Bombs, a whole load of 'em.
//!
//! This crate provides single-producer multi-consumer channel types, where
//! `Fuse`s are the producers and `Bomb`s are the consumers.
//!
//! [`Fuse`]/[`Bomb`]s are one-time use only, whereas [`MultiFuse`]/[`MultiBomb`]s can be reused, and store
//! values sent through the channel in a queue.
//!
//! For non-blocking channels, see the crate-level types.
//! For blocking channels, see the types in the [`blocking`] module.
//!
//! # Example
//!
//! If you want to send a "close program" signal to different threads,
//! such that each thread needs to clean up first before closing,
//! you can give each thread a [`Bomb<()>`](Bomb), and run cleanup code
//! once exploded.
//!
//! **Note:** This example assumes that your threads already run logic
//! in some event loop. It is advised to use the [`blocking`] variants instead
//! of checking `exploded()` in a loop.
//!
//! ```
//! use std::thread;
//! use bombs::blocking::Bomb;
//!
//! // Create a new fuse and bomb pair.
//! let (fuse, bomb) = Bomb::new();
//!
//! // Clone `bomb` into thread.
//! let bomb_clone = bomb.clone();
//! thread::spawn(move || {
//!     loop {
//!         // Do some stuff...
//!
//!         if let Some(_) = bomb_clone.exploded() {
//!             // Received close signal, break.
//!
//!             // Clean up data values...
//!
//!             break;
//!         }
//!     }
//! });
//!
//! // Create another thread.
//! // Move original `bomb` into thread.
//! thread::spawn(move || {
//!     loop {
//!         // Do some other stuff...
//!
//!         if let Some(_) = bomb.exploded() {
//!             // Received close signal, break.
//!
//!             // Clean up data values...
//!
//!             break;
//!         }
//!     }
//! });
//!
//! // Do some different stuff...
//!
//! // Send close signal.
//! let flame = fuse.light(());
//!
//! // Wait for all inner threads to close safely (checked by `Bomb` drop).
//! flame.wait_for_extinguish();
//!
//! // Now safely quit the program.
//! ```

mod types;

pub mod blocking;
mod basic; pub use basic::*;
